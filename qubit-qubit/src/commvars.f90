module commvars
!
implicit none
!
save
!
integer, parameter :: DP = selected_real_kind(14,200)
!integer, parameter :: DP = kind(1.0d0)

integer :: iverbose=1
integer :: stdout=6
integer, parameter :: ndim = 3, ndim1 = ndim+1
integer :: nthreads
!
real(DP), parameter :: IA(0:ndim) = (/ 2.d0, 0.d0, 0.d0, 0.d0 /)
real(DP) :: IAp(0:ndim)
!
real(DP), parameter :: eps(14) = (/ 1.d-1, 1.d-2, 1.d-3, 1.d-4, 1.d-5, 1.d-6, 1.d-7, 1.d-8, &
                                    1.d-9, 1.d-10, 1.d-11, 1.d-12, 1.d-13, 1.d-14 /) 
real(DP), parameter :: pi = acos(-1.d0)
!
logical :: lasym , gl_theta, lwrescl 
logical :: ltstates = .false.
logical :: lbloch = .true.
logical :: lb0 = .false.
!
character(len=80) :: winit, calculation
character(len=80) :: infile, outfile, pathfile
character(len=180) :: tempdir, cplexdir
character(len=2) :: steering
integer :: cplexmem=32000
!
real(DP) :: rho(0:ndim,0:ndim), invrho(0:ndim,0:ndim)
real(DP), allocatable :: thetas(:,:)
integer :: nstates
character(len=80), allocatable :: state_id(:)
!
integer :: nui,  p_lat, q_long, nvars
integer*8 :: c3nui
real(DP), allocatable :: ui(:,:), ui_inner(:,:), ui_outer(:,:), uia(:,:), dmu(:), wgl(:)
real(DP) :: r_u, r_u_lp, r_in, rlo, rup
integer :: iun(0:ndim,20)
integer*2, allocatable :: ind3(:,:) 
!
complex(DP), parameter :: one  = cmplx(1.d0,0.d0), &
                          zero = cmplx(0.d0,0.d0)
!
public :: DP, ndim, ndim1, nui, ui, uia, dmu, winit, rho, thetas, invrho, eps, &
          r_u, wgl, IA, IAp, iverbose, lbloch, lasym, c3nui, r_in, lb0, nstates, &
          ind3, nvars, r_u_lp, calculation, tempdir, cplexdir, p_lat, nthreads, &
          q_long, state_id, cplexmem, rup, rlo, ui_inner, ui_outer, iun, stdout, &
          steering

!
end module commvars
