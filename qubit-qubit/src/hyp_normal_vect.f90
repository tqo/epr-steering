subroutine plane_normal_vect2( p1, p2, p3, normal )
  !
  use commvars,  only : DP, ndim, ndim1, eps
  !
  implicit none
  !
  real(DP), intent(in) :: p1(0:ndim), p2(0:ndim), p3(0:ndim)
  real(DP), intent(out) :: normal(1:ndim1)
  ! last element is used for the offset
  !
  real(DP) :: v(2,1:ndim), norm
  integer :: i, j, k
  !
  ! two vectors in the plane
  !v(1,1:ndim) = p1(1:ndim)-p3(1:ndim)
  !v(2,1:ndim) = p2(1:ndim)-p3(1:ndim)
  normal(1:ndim) = p1(1:ndim)-p3(1:ndim)
  norm = sqrt(dot_product(normal(1:3),normal(1:3))); v(1,1:ndim) = normal(1:ndim)/norm
  if ( norm.lt.eps(8) ) write(*,*) "WARNING: two points on sphere too close"
  !
  normal(1:ndim) = p2(1:ndim)-p3(1:ndim)
  norm = sqrt(dot_product(normal(1:3),normal(1:3))); v(2,1:ndim) = normal(1:ndim)/norm
  if ( norm.lt.eps(8) ) write(*,*) "WARNING: two points on sphere too close"
  !
  ! cross product
  normal(1) = v(1,2)*v(2,3) - v(1,3)*v(2,2)
  normal(2) = v(1,3)*v(2,1) - v(1,1)*v(2,3)
  normal(3) = v(1,1)*v(2,2) - v(1,2)*v(2,1)
  !
  norm = sqrt(dot_product(normal(1:3),normal(1:3)))
  if ( norm.lt.eps(8) ) then
    write(*,*) "Three points are in the same line!", norm 
    write(*,*) normal(1:ndim)
    write(*,*)p1(0:ndim)
    write(*,*)p2(0:ndim)
    write(*,*)p3(0:ndim)
stop
  else
    normal(1:3) = normal(1:3) / norm
!    if ( normal(1).lt.0.d0 ) normal(1:3) = -normal(1:3)
  endif
  !
  normal(ndim1) = dot_product(normal(1:ndim),p1(1:ndim))
  !
end subroutine plane_normal_vect2
!
subroutine plane_normal_vect( points_coor, normal )
  !
  use commvars,  only : DP, ndim, ndim1, eps
  !
  implicit none
  !
  real(DP), intent(in) :: points_coor(0:ndim,0:ndim)
  real(DP), intent(out) :: normal(1:ndim1)
  ! last element is used for the offset
  !
  real(DP) :: v(2,1:ndim), norm
  integer :: i, j, k
  !
  ! two vectors in the plane
  !v(1,1:ndim) = points_coor(1,1:ndim)-points_coor(ndim,1:ndim)
  !v(2,1:ndim) = points_coor(2,1:ndim)-points_coor(ndim,1:ndim)
  normal(1:ndim) = points_coor(1,1:ndim)-points_coor(ndim,1:ndim)
  norm = sqrt(dot_product(normal(1:3),normal(1:3))); v(1,1:ndim) = normal(1:ndim)/norm
  normal(1:ndim) = points_coor(2,1:ndim)-points_coor(ndim,1:ndim)
  norm = sqrt(dot_product(normal(1:3),normal(1:3))); v(2,1:ndim) = normal(1:ndim)/norm
  !
  ! cross product
  normal(1) = v(1,2)*v(2,3) - v(1,3)*v(2,2)
  normal(2) = v(1,3)*v(2,1) - v(1,1)*v(2,3)
  normal(3) = v(1,1)*v(2,2) - v(1,2)*v(2,1)
  !
  norm = sqrt(dot_product(normal(1:3),normal(1:3)))
  if ( norm.lt.eps(8) ) then
    write(*,*) "Three points are in the same line!" 
  else
    normal(1:3) = normal(1:3) / norm
  endif
  !
  normal(ndim1) = dot_product(normal(1:ndim),points_coor(1,1:ndim))
  !
end subroutine plane_normal_vect
!
subroutine hyp_normal_vect( points_coor, normal )
  !
  use commvars,  only : DP, ndim, ndim1, eps
  !
  implicit none
  !
  real(DP), intent(in) :: points_coor(0:ndim,0:ndim)
  real(DP), intent(out) :: normal(0:ndim1)
  ! last element is used for the offset
  real(DP), external :: det3
  !
  !real(DP) :: v1(0:ndim), v2(0:ndim), v3(0:ndim)
  real(DP) :: v(3,0:ndim), norm
  integer :: i, j, k
  !
  normal(0:ndim) = points_coor(1,0:ndim)-points_coor(0,0:ndim)
  norm = sqrt(dot_product(normal(0:3),normal(0:3))); v(1,0:ndim)=normal(0:ndim)/norm
  normal(0:ndim) = points_coor(2,0:ndim)-points_coor(0,0:ndim)
  norm = sqrt(dot_product(normal(0:3),normal(0:3))); v(2,0:ndim)=normal(0:ndim)/norm
  normal(0:ndim) = points_coor(3,0:ndim)-points_coor(0,0:ndim)
  norm = sqrt(dot_product(normal(0:3),normal(0:3))); v(3,0:ndim)=normal(0:ndim)/norm
  !
  normal(0) =  det3( v(:,1), v(:,2), v(:,3) )
  normal(1) = -det3( v(:,0), v(:,2), v(:,3) )
  normal(2) =  det3( v(:,0), v(:,1), v(:,3) )
  normal(3) = -det3( v(:,0), v(:,1), v(:,2) )
  !
  norm = sqrt(dot_product(normal(0:3),normal(0:3)))
  if ( norm.lt.eps(8) ) then
    write(*,*) "Four points are on the same 3D plane!", norm
    write(*,*)normal(0:ndim)
    write(*,*)points_coor(0,0:ndim)
    write(*,*)points_coor(1,0:ndim)
    write(*,*)points_coor(2,0:ndim)
    write(*,*)points_coor(3,0:ndim)
    stop
  else
    normal(0:3) = normal(0:3) / norm
  endif
  !normal(0:3) = normal(0:3)/sqrt(dot_product(normal(0:3),normal(0:3)))
  !
  normal(ndim1) = dot_product(normal(0:ndim),points_coor(1,0:ndim))
  !
  !
end subroutine hyp_normal_vect
!
!
!
function det3 ( A1, A2, A3 )
  ! A1, A2, A3: 3 column vectors that forms a 3x3-matrix A
  ! det: determinant of A
  !
  use commvars,  only: DP
  !
  implicit none
  !
  real(DP), intent(in) :: A1(3), A2(3), A3(3)
  real(DP) :: det3
  !
  det3 = A1(1)*A2(2)*A3(3) + &
         A1(2)*A2(3)*A3(1) + &
         A1(3)*A2(1)*A3(2) - &
         A3(1)*A2(2)*A1(3) - &
         A3(2)*A2(3)*A1(1) - &
         A3(3)*A2(1)*A1(2)
  !
end function det3
