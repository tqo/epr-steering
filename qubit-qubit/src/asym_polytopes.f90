subroutine asym_polytopes( )
  !
  ! generate inner polyhedrons to approximate the Bloch sphere 
  !
  use commvars, only : DP, eps, pi, ndim, nui, ui,  ui_inner, ui_outer, &
                       p_lat, q_long, nvars, r_in, wgl, rho, dmu, stdout
  !
  implicit none
  !
  real(DP) :: u, v, theta, phi
  real(DP) :: a, b, tt
  real(DP), allocatable :: gl(:)
  integer :: i, j, k, l
  !
  !
  if ( nui .eq. 114 ) then
    !
    p_lat = 7; q_long = 16
    r_in = 0.960238742158d0
    !
  else if ( nui .eq. 182 ) then
    !
    p_lat = 9; q_long = 20
    r_in = 0.974578062776d0
    !
  else if ( nui .eq. 266 ) then
    !
    p_lat = 11; q_long = 24
    r_in = 0.982383137658d0
    !
  else if ( nui .eq. 366 ) then
    !
    p_lat = 13; q_long = 28
    r_in = 0.987085694494d0
    !
  else if ( nui .eq. 482 ) then
    !
    p_lat = 15; q_long = 32
    r_in = 0.990132751783d0
    !
  else if ( nui .eq. 614 ) then
    !
    p_lat = 17; q_long = 36
    r_in = 0.992217866337d0
    !
  else if ( nui .eq. 762 ) then
    !
    p_lat = 19; q_long = 40
    r_in = 0.993706567289d0
    !
  else if ( nui .eq. 926 ) then
    !
    p_lat = 21; q_long = 44
    r_in = 0.994806122713d0
    !
  else if ( nui .eq. 1106 ) then
    !
    p_lat = 23; q_long = 48
    r_in = 0.995641091987d0
    !
  else if ( nui .eq. 1302 ) then
    !
    p_lat = 25; q_long = 52
    r_in = 0.996289953453d0
    !
  else if ( nui .eq. 1514 ) then
    !
    p_lat = 27; q_long = 56
    r_in = 0.996804129735d0
    !
  endif
  !
  nvars = p_lat + 2
  allocate( ui(0:ndim,nui), ui_inner(0:ndim,nui), ui_outer(0:ndim,nui) )
  allocate( gl(p_lat), wgl(p_lat) )
  call gl_weights( p_lat, gl, wgl )
  !
  do i = 1, p_lat
    !
    phi = 0.d0
    theta = acos(gl(i)) 
    !
    ui(0,i) = 1.d0
    ui(1,i) = sin(theta)*cos(phi)
    ui(2,i) = sin(theta)*sin(phi)
    ui(3,i) = cos(theta) 
    !
    !write(*,'(3(F17.12))') ui(1:3,i)
    !
  enddo
  !
  deallocate( gl )
  !
  do i = 1, q_long-1
    theta = 2.d0*pi/dble(q_long)*dble(i)
    u = cos(theta); v = sin(theta)
    do j = 1, p_lat
      ui(0,i*p_lat+j) = ui(0,j)
      ui(1,i*p_lat+j) = u*ui(1,j) - v*ui(2,j)
      ui(2,i*p_lat+j) = v*ui(1,j) + u*ui(2,j)
      ui(3,i*p_lat+j) = ui(3,j)
    enddo
  enddo
  !
  ui(0,nui) = 1.d0; ui(0,nui-1) = 1.d0
  ui(1,nui) = 0.d0; ui(1,nui-1) = 0.d0
  ui(2,nui) = 0.d0; ui(2,nui-1) = 0.d0
  ui(3,nui) = 1.d0; ui(3,nui-1) =-1.d0
  !
  ! check to make sure that all the points are on Bob's positive cone 
  !
  do i = 1, nui
  !write(*,'(4(F17.12))')ui(0:ndim,i)
    u = ui(0,i)**2 - dot_product( ui(1:ndim,i),ui(1:ndim,i) )
    if ( abs(u).gt.eps(8) ) then
      write(stdout,*)"Not on Bob's positive cone", i
      write(stdout,'(4(F17.12))') ui(0:ndim,i)
      stop
    endif
  enddo
  !
  ui_inner(:,:) = ui(:,:); ui_outer(:,:) = ui(:,:)
  do i = 1, nui
    ui_outer(1:3,i) = ui_outer(1:3,i) / r_in
  enddo
  !
  write(stdout,'(7x,A)') "- polytope with axial symmetry: Gauss-Legendre abscissae on z-axis"
  write(stdout,'(9x,A,I2,A,I2,A,I4,A)') "(p = ",p_lat,", q = ", q_long, ", N = ", nui,")"
  !
end subroutine asym_polytopes
