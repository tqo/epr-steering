subroutine gen_polytopes
  !
  use commvars, only : ndim, nui, ui, dmu,  calculation, &  
                       lasym, nvars, c3nui, lbloch, r_in, stdout
  !
  implicit none
  integer :: i, k, l
  character(len=80) :: calc
  character(len=1), external :: capital 
  !
  !write(stdout,'(/5X,A)') "Polytopes LHS DISTRIBUTION INFO: "
  !
  if ( lasym  ) then
    !
    write(stdout,'(/5X,"The Bloch sphere is approximated by ",I4,"-vertex polytopes")')nui
    write(stdout,'( 5X,"WARNING: this polytope is valid for states with AXIAL SYMMETRY only!!!")')
    write(stdout,'( 5X,"         PLEASE VERIFY THAT THIS IS WHAT YOU REALLY WANT!!!")')
    call asym_polytopes( )
    call check_invsym( )
    !
  else
    !
    write(stdout,'(/5X,"The Bloch sphere is approximated by ",I4,"-vertex polytopes")')nui
    call isym_polytopes( )
    call check_invsym( )
    !
  endif
  !
  if ( .not.lbloch ) then
    do i = 1, nui
      ui(1:3,i) = ui(1:3,i) / r_in
    enddo
  endif
  !
  if ( lasym ) then
    call setup_ind3_asym( )
  else
    call setup_ind3_isym( )
  endif
  !
  !
end subroutine gen_polytopes
!
!
subroutine check_invsym( )
  !
  use commvars, only : DP, eps, ndim, nui, ui, stdout
  !
  implicit none
  !
  logical :: linvsym
  integer, allocatable :: ind(:)
  integer :: i, j, nup, ndwn
  real(DP) :: u, uiui, uiuj
  !
  linvsym = .true.
  !
  allocate( ind(nui) ); ind(:) = 0
  do i = 1, nui
    if ( ind(i).ne.0 ) cycle
    ind(i) = 1
    uiui = dot_product( ui(1:ndim,i),ui(1:ndim,i) )
    do j = i+1, nui
      if ( ind(j).gt.0 ) then
        cycle
      else
        u = dot_product( ui(1:ndim,i),ui(1:ndim,j) ) 
        !if ( u.lt.0 .and. abs(1.d0-abs(u)).lt.eps(8) ) then
        if ( u.lt.0 .and. abs(uiui-abs(u)).lt.eps(8) ) then
          ind(j) = -1
          !write(*,*)ui(1:ndim,i)
          !write(*,*)ui(1:ndim,j)
          !write(*,*)"-----------"
          exit
        endif
      endif
    enddo
  enddo
  !
  nup = 0; ndwn = 0
  do i = 1, nui
    if ( ind(i).eq.1 ) nup = nup + 1
    if ( ind(i).eq.-1 ) ndwn = ndwn + 1
  enddo
  if ( nup.ne.ndwn ) linvsym = .false.
  !
  if ( linvsym ) then
    write(stdout,'(7x,"- inversion symmetry found")')
  else
    write(stdout,'(7x,"- no inversion symmetry ")')
  endif
  !
end subroutine check_invsym
!
!
subroutine setup_ind3_asym( )
  !
  use commvars,  only: DP, ndim, ndim1, eps, nui, ind3, c3nui, ui, &
                       q_long, p_lat, tempdir, stdout 
  !
  implicit none
  !
  integer*8 :: p, q
  real(DP) :: normal(1:ndim1), d1
  integer*1, allocatable :: ind(:)
  integer :: i, i1, ii, j, j1, jj, k, k1, kk, l, np
  logical :: lind3
  character(len=80) :: filname
  !
  integer*8, external :: nijk
  character(len=8), external :: int_to_char
  !
  ! check if the tast has been done before
  !
  filname=trim(tempdir)//'ind3_p'//trim(int_to_char(p_lat))//&
          '_q'//trim(int_to_char(q_long))//'.dat'
  inquire( file=filname, exist=lind3 )
  !
  if ( lind3 ) then
    !
    open( 1, file=filname, action='read' )
    rewind(1)
    read(1,*)c3nui
    allocate( ind3(c3nui,0:3) ); ind3(:,:) = 0 ! important initialization
    read(1,*)ind3
    close(1)
    !
  else
    !
    ! for arrangements of points with axial symmetry, there are many 
    ! equivalent planes. Note that coplanar points are taken
    ! care of automatically
    !
    !c3nui = nui*(nui-1)*(nui-2)/6
    c3nui = nui; p = nui-1; q = nui-2; c3nui = c3nui*p*q/6
    allocate( ind(0:c3nui) ); ind(:) = 0 !important initialization
    !
    p = 0
    do i = 1, nui
      do j = i+1, nui
        do k = j+1, nui
          !
          p = p + 1
          !
          if ( ind(p).eq.0) then
            do l = 1, q_long-1
              i1 = i + l*p_lat; if ( i1.gt.nui ) i1 = i1 - nui
              j1 = j + l*p_lat; if ( j1.gt.nui ) j1 = j1 - nui
              k1 = k + l*p_lat; if ( k1.gt.nui ) k1 = k1 - nui
              q = nijk(i1,j1,k1,nui); !if (q.gt.p) ind(q) = 1
              !
              if ( q.lt.0 .or. q.gt.c3nui ) then
                write(stdout,*)" wrong q", q
                stop
              else
                if (q.gt.p) ind(q) = 1
              endif
              q = nijk(i1,k1,j1,nui); !if (q.gt.p) ind(q) = 1
              !
              if ( q.lt.0 .or. q.gt.c3nui ) then
                write(stdout,*)" wrong q", q
                stop
              else
                if (q.gt.p) ind(q) = 1
              endif
              q = nijk(j1,i1,k1,nui); !if (q.gt.p) ind(q) = 1
              !
              if ( q.lt.0 .or. q.gt.c3nui ) then
                write(stdout,*)" wrong q", q
                stop
              else
                if (q.gt.p) ind(q) = 1
              endif
              q = nijk(j1,k1,i1,nui); !if (q.gt.p) ind(q) = 1
              !
              if ( q.lt.0 .or. q.gt.c3nui ) then
                write(stdout,*)" wrong q", q
                stop
              else
                if (q.gt.p) ind(q) = 1
              endif
              q = nijk(k1,i1,j1,nui); !if (q.gt.p) ind(q) = 1
              !
              if ( q.lt.0 .or. q.gt.c3nui ) then
                write(stdout,*)" wrong q", q
                stop
              else
                if (q.gt.p) ind(q) = 1
              endif
              q = nijk(k1,j1,i1,nui); !if (q.gt.p) ind(q) = 1
              !
              if ( q.lt.0 .or. q.gt.c3nui ) then
                write(stdout,*)" wrong q", q
                stop
              else
                if (q.gt.p) ind(q) = 1
              endif
              !
            enddo
          endif
          !
        enddo
      enddo
    enddo
    !
    q = 0
    do p = 1, c3nui
      if ( ind(p).eq.0 ) q = q + 1
    enddo
    !write(*,*)"q = ", q
    !
    c3nui = q; q = 0; p = 0
    allocate( ind3(c3nui,0:3) ); ind3(:,:) = 0 ! important initialization
    do i = 1, nui
      do j = i+1, nui
        do k = j+1, nui
          !
          p = p + 1
          !
          if ( ind(p).eq.0) then
            q = q + 1; ind3(q,1) = i; 
            ind3(q,2) = j; ind3(q,3) = k
          endif
        enddo
      enddo
    enddo
    !
    !stop
    !write(*,*)
    !do i = 1, nui
    !  write(*,'(A,4(F17.12))')"u"//trim(int_to_char(i)),ui(0:ndim,i)
    !enddo
    !
    if ( q.ne.c3nui ) then
      write(stdout,*)"setup_ind3_cylsym: q /= c3nui"
      stop
    endif
    !
    open( 1, file=filname, action='write' )
    rewind(1)
    write(1,*)c3nui
    write(1,*)ind3
    close(1)
    !
    deallocate( ind )
    !
  endif
  !
  write(stdout,'(7x,"- inequivalent classes of planes: ",I12)') c3nui
  !
end subroutine setup_ind3_asym
!
!
subroutine setup_ind3_isym( )
  !
  use commvars,  only: DP, ndim, ndim1, eps, nui, ind3, c3nui, ui, stdout
                       
  !
  implicit none
  !
  integer*8 :: p, q
  real(DP) :: normal(1:ndim1), d1
  integer :: ind(nui)
  integer :: i, i1, ii, j, j1, jj, k, k1, kk, l, np
  !
  integer*8, external :: nijk
  !
  ! set up index array taking into account the fact that
  ! more than 3 points can be on the same plane
  !
  c3nui = nui*(nui-1)*(nui-2)/6
  allocate( ind3(0:c3nui,0:3) ); ind3(:,:) = 0 ! important initialization
  !
  p = 0
  do i = 1, nui
    do j = i+1, nui
      do k = j+1, nui
        !
        p = p + 1; ind3(p,1) = i; 
        ind3(p,2) = j; ind3(p,3) = k
        !
        if ( ind3(p,0).eq.0 ) then
          ! check points that are coplanar with these 3 points
          ! equation of plane first
          call plane_normal_vect2( ui(0:ndim,i), ui(0:ndim,j), ui(0:ndim,k), normal(1:ndim1) )
          !
          ! check coplanar points
          np = 3; ind(1) = i; ind(2) = j; ind(3) = k
          do l = k+1, nui
            if ( l.eq.i .or. l.eq.j .or. l.eq.k ) cycle
            d1 = dot_product( normal(1:ndim), ui(1:ndim,l) ) - normal(ndim1)
            if ( abs(d1).lt.eps(10) ) then
              np = np + 1; ind(np) = l
            endif
            if ( np.gt.nui ) then
              write(stdout,*)"compute_constraints: ind array out-of-bound"
              stop
            endif
          enddo
          !
          if ( np.gt.3 ) then
            do i1 = 1, np
              do j1 = i1+1, np
                do k1 = j1+1, np
                  !write(*,*)nijk(ind(i1),ind(j1),ind(k1),nui)
                  q = nijk(ind(i1),ind(j1),ind(k1),nui)
                  if ( (q.lt.0) .or. (q.gt.c3nui) ) then
                    write(stdout,*) "1: nijk < 0", q, ind(i1),ind(j1),ind(k1)
                    stop
                  else
                    ind3(q,0) = 1
                  endif
                enddo
              enddo
            enddo
            !
            ind3(p,0) = 0
            !
          endif
          !
        endif
        !
      enddo
    enddo
  enddo
  !
  !stop
  !write(*,*)
  !do i = 1, nui
  !  write(*,'(A,4(F17.12))')"u"//trim(int_to_char(i)),ui(0:ndim,i)
  !enddo
  !
  if ( p.ne.c3nui ) then
    write(stdout,*)"setup_ind3_isym: p /= c3nui"
    stop
  endif
  !
  q = 0
  do p = 1, c3nui
    if ( ind3(p,0).eq.0 ) then
      q = q + 1
      ind3(q,0:3) = ind3(p,0:3)
    endif
  enddo
  c3nui = q
  write(stdout,'(7x,"- inequivalent classes of planes: ",I12)') c3nui
  !
end subroutine setup_ind3_isym
!
!
!
!
!
function nijk( m, n, p, nui )
  !
  implicit none
  !
  integer, intent(in) :: m, n, p, nui
  integer*8 :: nijk
  !
  integer :: i, j, k
  !
  integer*8, external :: nijk_fixed
  !
  if ( n.le.m .or. p.le.n ) then
    nijk = 0; return
  endif
  ! this function computes
  ! nijk = \sum_{i=1}^{m} \sum_{j=i+1}^{n} \sum_{k=j+1}^{p} 1 
  ! which is used for mapping indices i,j,k of 3d array to an 1d array
  !
  !nijk_fixed(m,n,p) = (2*m + 3*m*m + m*m*m - 3*m*n - 3*m*n*n - 3*m*p - 3*m*m*p + 6*m*n*p)/6
  !
  nijk = nijk_fixed(m-1,nui,nui)
  nijk = nijk + nui*(n-m-1) - (n-1)*n/2 + (m+1)*m/2 + (p-n)
  !write(*,*) m, n, p, nijk
  !stop
  return
  !
  ! more stupid way, but it works for sure 
  !
  nijk = 0
  do i = 1, m-1
    do j = i+1, nui
      do k = j+1, nui
        nijk = nijk + 1
      enddo
    enddo
  enddo
  !
  i = m
  do j = i+1, n-1
    do k = j+1, nui
      nijk = nijk + 1
    enddo
  enddo
  !
  j = n
  do k = j+1, p
    nijk = nijk + 1
  enddo
  !
end function nijk
!
!
function nijk_fixed( m, n, p )
  !
  implicit none
  !
  integer, intent(in) :: m, n, p
  integer*8 :: m_, n_, p_
  integer*8 :: nijk_fixed
  !
  integer :: i, j, k
  !
  ! this function computes
  ! nijk = \sum_{i=1}^{m} \sum_{j=i+1}^{n} \sum_{k=j+1}^{p} 1 
  !        ( m, n, p are fixed )
  ! which is used for mapping indices i,j,k of 3d array to an 1d array
  !
  !nijk_fixed = (2*m + 3*m*m + m*m*m - 3*m*n - 3*m*n*n - 3*m*p - 3*m*m*p + 6*m*n*p)/6
  m_ = m; n_ = n; p_ = p
  nijk_fixed = (2*m_ + 3*m_*m_ + m_*m_*m_ - 3*m_*n_ - 3*m_*n_*n_ - 3*m_*p_ - 3*m_*m_*p_ + 6*m_*n_*p_)/6
  !
end function nijk_fixed

subroutine compute_r_inscribed_qhull( )
  !
  use commvars,  only: DP, ndim, nui, ui
  !
  implicit none
  !
  integer :: i, nfacets
  real(DP) :: x, y, z, offset, r_in = 100000000000.d0
  character(len=132) :: cmd
  integer :: cstat, estat
  character(len=100) :: cmsg
  !
  open( 1, file="lhs.in.txt", status='unknown', form='formatted', action='write' )
  write(1,'(I1)') ndim
  write(1,'(I4)') nui
  do i = 1, nui
    write(1,'(3(F17.12))') ui(1,i), ui(2,i), ui(3,i) 
  enddo
  close(1)
  !
  cmd="/home/iop.nhviet/.bin/qhull n < lhs.in.txt > normal-offset.txt"
  call execute_command_line ( trim(cmd) , EXITSTAT=estat, &
                             CMDSTAT=cstat, CMDMSG=cmsg)
  !
  open( 1, file="normal-offset.txt", status='unknown', form='formatted', action='read' )
  rewind(1); read(1,*) i; read(1,*) nfacets
  do i = 1, nfacets
    read(1,*) x, y, z, offset
    if ( abs(offset).lt.r_in ) r_in = abs(offset)
  enddo
  close(1, status="delete")
  !
  write(*,'(3x,A,I5,f18.12)') "- INSCRIBED RADIUS by qhull r_in: ", nui, r_in
  !
end subroutine compute_r_inscribed_qhull
