# created by hands :-) 
#
# gfortran compiler
FC = gfortran 
LD = gfortran 
FFLAGS = -fopenmp -fmax-stack-var-size=1024000
LDFLAGS = -fopenmp -fmax-stack-var-size=1024000
#
# uncomment four lines below to use intel fortran compiler
#
#FC = ifort 
#LD = ifort 
#FFLAGS         = -qopenmp -heap-arrays 1024 -O2 -assume byterecl -g -traceback
#LDFLAGS        = -qopenmp -heap-arrays 1024 -static-intel 
